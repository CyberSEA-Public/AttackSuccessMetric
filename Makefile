# Compiler and Flags
HS = ghc
HSFLAGS = -O2 --make -w

# Sources, Objects, Binaries, and Targets
HS_SRC =                    \
	Behaviour.hs            \
	Stimulus.hs             \
	ConcreteSpec.hs   	    \
	NextBehaviour.hs        \
	NextStimulus.hs         \
	Agent.hs                \
	AttackSuccessParser.hs  \
	AttackSuccessReader.hs  \
	AttackSuccess.hs

SRC_DIR = SourceCode

SOURCES = $(patsubst %,$(SRC_DIR)/%,$(HS_SRC))

BINARY = AttackSuccess

TARGETS = $(BINARY)

AUX = $(SRC_DIR)/*.o $(SRC_DIR)/*.hi $(SRC_DIR)/*~ #$(SRC_DIR)/*.hs

# Build All
all : $(TARGETS)

# Build Binary Target
$(BINARY) : $(SOURCES)	
	$(HS) $(HSFLAGS) $^ -o $@ 

# Clean Up	
clean : 
	rm -f $(BINARY) $(AUX)
