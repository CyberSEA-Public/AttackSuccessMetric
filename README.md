# An Attack Success Metric for Implicit Interactions in Distributed Systems 

A Haskell implementation computing an Attack Success Metric for Implicit Interactions based on C²KA specifications. 


## Compilation

To compile the code: please use the provided Makefile. This can be doen by typing `make` at a terminal. This will compile a binary executable called `AttackSuccess`.

## Executing the Code

To run the compiled executable, type the following at a terminal:

	./AttackSuccess <System Specification Folder>
	
where `<System Specification Folder>` is the name of the system folder in the `/Examples` directory. 
	
The `<System Specification Folder>` must contain the following files:

* **AgentList.txt**: A list of every `AgentName` of the system, with one name per line; ensure that there are no extra characters and no empty lines.

* **Implicits.txt**: A list of every implicit interaction found to exist in the system specification, with one implicit interaction per line with the following format: 
	
	`AgentName CommunicationType AgentName CommunicationType ...`

	where the `CommunicationType` is `S` for stimulus or `E` for environment and the `AgentNames` are the same as those in `AgentList.txt`; Every implicit interaction is given in reverse order to facilitate simpler computation. For example, an implicit interaction `A →S B →E C` would be written as `C E B S A`.  

* **AgentName.txt**: For each `AgentName' in the system specification, a file containing the C²KA specification fo the agent must be provided. 

The format of the agent specifications for each AgentName.txt is as follows:  
  
	begin AGENT where  
	  AgentName := CKAExpr  
	end
  
	begin NEXT_BEHAVIOUR where  
    	(Stim,CKA) = CKA  
	end  
  
	begin NEXT_STIMULUS where  
    	(Stim,CKA) = Stim  
	end  
  
	begin CONCRETE_BEHAVIOUR where  
		CKA => [ Stmt ]  
	end

As an example, to compute the Attack Success Metric for the implicit interactions found to exist in the provided Distributed Manufacturing Cell Control System example, type the following at a terminal:

	./AttackSuccess ManufacturingCell