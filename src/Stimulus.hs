{- AUTHOR: Jason Jaskolka -}
-- Module containing the type definitions and parsers for stimuli

module Stimulus  ( Stim          ,
                   StimSet       ,
                   StimExpr (..) ,
				   parseStimExpr ) where

{- IMPORTS -}
import Data.Set ( Set )
import Control.Applicative  ( (<*) )
import Text.Parsec          ( alphaNum ,
                              eof      ,
                              letter   ,
                              parse    ,
                              oneOf    , 
                              (<?>)    , 
                              (<|>)      )
import Text.Parsec.Expr     ( Assoc (AssocLeft)     , 
                              Operator (Infix)      ,
                              buildExpressionParser   )
import Text.Parsec.Language ( emptyDef )
import Text.Parsec.String   ( Parser )							  
import Text.Parsec.Token    ( GenLanguageDef (..) ,
                              GenTokenParser (..) ,
                              makeTokenParser       )

{- DATA TYPES -}
type Stim = String	

type StimSet = Set Stim	

data StimExpr = 
	D                          |
	N                          |
	Literal Stim               |
	Choice  StimExpr StimExpr  | 
	Compose StimExpr StimExpr
		deriving (Eq)		



{- PARSER -}
-- StimExpr language grammar
grammar = emptyDef{ identStart      = letter                           ,
                    identLetter     = alphaNum                         ,
                    opStart         = oneOf "+*:"                      ,
                    opLetter        = oneOf "+*"                       ,
                    reservedOpNames = ["++", "\8853", "**", "\8857"]    ,
                    reservedNames   = ["N", "\120211", "D", "\120201"]   }		

TokenParser{ parens     = m_parens     , 
             identifier = m_identifier , 
             reservedOp = m_reservedOp ,
             reserved   = m_reserved   ,
             semiSep1   = m_semiSep1   ,
             whiteSpace = m_whiteSpace   } = makeTokenParser grammar

exprParser :: Parser StimExpr
exprParser = buildExpressionParser table term <?> "StimExpr"
	where
		table = [ [Infix (m_reservedOp "++"     >> return (Choice))  AssocLeft] ,
		          [Infix (m_reservedOp "\8853" >> return (Choice))  AssocLeft] ,
		          [Infix (m_reservedOp "**"    >> return (Compose)) AssocLeft] ,
		          [Infix (m_reservedOp "\8857" >> return (Compose)) AssocLeft]   ]
		term  = m_parens exprParser
		        <|> fmap Literal m_identifier
		        <|> (m_reserved "N"       >> return (N))
		        <|> (m_reserved "\120211" >> return (N))
		        <|> (m_reserved "D"       >> return (D))	
		        <|> (m_reserved "\120201" >> return (N))

mainParser :: Parser StimExpr
mainParser = m_whiteSpace >> exprParser <* eof	

parseStimExpr :: String -> StimExpr
parseStimExpr input = case parse mainParser "" input of
	Left  err -> error $ show err
	Right ans -> ans   			  