{- AUTHOR: Quentin Yang -}
-- Module to read the agents and lists of implicit interactions for the Attack Success Metric computation

module AttackSuccessReader ( Rule ( .. )     ,
                			 VarSet          ,
               			 	 CommType ( .. ) ,
               			 	 buildRule       ,
							 getNextStimSet  ,
							 getNextBehaSet  ,
							 getInfV         ,
							 getInfS         ,
							 getRefV          ) where

{-  IMPORTS -}
import Agent( Agent ( .. ) ,
              AgentSpec    ,
              AgentName    ,
              nsmap        ,
			  nbmap        ,
			  expr         , 
			  load          )
import Behaviour ( CKA, CKASet, extractCKA )
import Stimulus ( Stim, StimSet )
import ConcreteSpec ( VarName )
import NextStimulus ( NSExpr ( NS ) )
import NextBehaviour ( NBExpr ( NB ) )

import AttackSuccessParser ( parseSpec, Expr (.. ), Cond ( .. ), Stmt ( .. ), Guard ( .. ) )

import Control.Exception ( tryJust )
import Control.Monad ( guard )
import System.IO.Error ( isDoesNotExistError )
import Data.List
import qualified Data.Set as Set
import qualified Data.Map as HM

{-  DATA TYPES -}
type VarSet = Set.Set VarName

data CommType = S | E
    deriving ( Show )

data Rule = R {
        lookupAgent :: AgentName -> Agent,
        available :: Agent -> CKASet,
        refV :: CKA -> VarSet,
        defV :: CKA -> VarSet,
        infS :: CKA -> StimSet,
        nextStim :: Stim -> CKA -> Stim,
        nextBeha :: Stim -> CKA -> CKA,
        inflV :: String -> CommType -> CKA -> VarSet,
        stimList :: [Stim],
        probDistrib :: Agent -> CKA -> Double,
        probAffect :: Agent -> (VarSet->Double) -> VarSet -> Double
        }


{- GETTERS -}
getNextStimSet :: Stim -> Agent -> Rule -> StimSet
getNextStimSet stim agent rule = Set.map ((nextStim rule) stim) ((available rule) agent)

getNextBehaSet :: Stim -> Agent -> Rule -> CKASet
getNextBehaSet stim agent rule = Set.map ((nextBeha rule) stim) ((available rule) agent)

getInfV :: (Set.Set String) -> CommType -> Agent -> Rule -> VarSet
getInfV set S agent rule = do
    let f s cka varset = Set.union ((inflV rule) s S cka) varset
    let g s varset = Set.union varset $ Set.foldr (f s) Set.empty $ (available rule) agent
    Set.foldr g Set.empty set
getInfV set E agent rule = do
    let f v cka varset = Set.union ((inflV rule) v E cka) varset
    let g v varset = Set.union varset $ Set.foldr (f v) Set.empty $ (available rule) agent
    Set.foldr g Set.empty set

getInfS :: Rule -> Agent -> StimSet
getInfS rule agent = Set.foldr (foldInlfS (infS rule)) Set.empty ((available rule) agent)
        where
                foldInlfS inflS a stimset = Set.union stimset $ inflS a

getRefV :: Rule -> Agent -> VarSet
getRefV rule agent = foldr myCKAfold Set.empty ((available rule) agent)
        where
                myCKAfold a set = Set.union (refV rule a) set

{- CONSTRUCTOR -}
-- I/O construction of the probability distribution.
buildProbDistrib :: FilePath -> IO (Agent -> CKA -> Double)
buildProbDistrib dir = do
        string <- readFile $ "../specs/" ++ dir ++ "/probabilityDistribution.txt"
        let hm = aux $ map words $ lines string
        let f agent behavior = case (HM.lookup (name agent) hm) of
                Nothing -> error $ "Agent " ++ (name agent) ++ " does not exists!"
                Just h -> case (HM.lookup behavior h) of
                        Nothing -> 0.0
                        Just p -> p
        return f
        where
                aux [] = HM.empty
                aux (ligne:q) = do
                        let h = aux q
                        let agent = head ligne
                        HM.insert agent (insertBehavior (tail ligne) HM.empty) h
                insertBehavior [] h = h
                insertBehavior [_] h = h
                insertBehavior (b:q) h = do
                        insertBehavior (tail q) (HM.insert b (read $ head q) h)

-- I/O construction of the agents
-- Map an Agent to its name.
mapAgentName :: [AgentName] -> FilePath -> IO(AgentName -> Agent)
mapAgentName liste dir = do
        hashmap <- aux liste dir
        let f nom = case (HM.lookup nom hashmap) of
                Nothing -> error $ "Agent " ++ nom ++ " does not exist!"
                --This case should never happen since the argument [AgentName] is supposed to be correct
                Just a -> a
        return f
        where
                aux [] dir = return HM.empty
                aux (nom:q) dir = do
                        agent <- load $ dir ++ nom ++ ".txt"
                        h <- aux q dir
                        return $ HM.insert nom agent h

-- Derivation of the memoized functions
buildAvailable :: [Agent] -> (Agent -> CKASet)
buildAvailable liste = do
        let hashmap = aux liste
        let f a = case (HM.lookup (name a) hashmap) of
                Nothing -> Set.empty -- the specified agent is not a part of the system
                Just set -> set
        f
        where
                aux [] = HM.empty
                aux (a:q) = HM.insert (name a) (extractCKA $ expr $ spec $ a) $ aux q

buildNextStimMap :: [Agent] -> (Stim -> CKA -> Stim)
buildNextStimMap liste = do
        let hashmap = aux liste HM.empty
        let f s a = case (HM.lookup a hashmap) of
                Nothing -> "N"
                Just stimap -> case (HM.lookup s stimap) of
                        Nothing -> "N"
                        Just stim -> stim
        f
        where
                aux [] h = h
                aux (a:q) h =
                        browse (aux q h) $ nsmap $ spec a
                browse h [] = h
                browse h ((NS stim a s):q) = do
                        let hm = browse h q
                        let stimap = case (HM.lookup a hm) of
                                Nothing -> HM.empty
                                Just hmap -> hmap
                        HM.insert a (HM.insert stim s stimap) hm

buildNextBehaMap :: [Agent] -> (Stim -> CKA -> CKA)
buildNextBehaMap liste = do
        let hashmap = aux liste HM.empty
        let f s a = case (HM.lookup a hashmap) of
                Nothing -> "Z"
                Just stimap -> case (HM.lookup s stimap) of
                        Nothing -> "Z"
                        Just b -> b
        f
        where
                aux [] h = h
                aux (a:q) h = browse (aux q h) $ nbmap $ spec a
                browse h [] = h
                browse h ((NB stim a b):q) = do
                        let hm = browse h q
                        let stimap = case (HM.lookup a hm) of
                                Nothing -> HM.empty
                                Just hmap -> hmap
                        HM.insert a (HM.insert stim b stimap) hm

buildStimList :: [Agent] -> [Stim]
buildStimList liste = case liste of
        [] -> []
        (t:q) -> Set.toList $ deriveStimSet $ nsmap $ spec t
        where
                deriveStimSet [] = Set.empty
                deriveStimSet ((NS s a stim):queue) = Set.insert s $ deriveStimSet queue
               
-- I/O construction of the specification mapping
buildCBmap :: [AgentName] -> FilePath -> IO (HM.Map CKA Stmt)
buildCBmap [] dir = return HM.empty
buildCBmap (name:q) dir = do
        h <- buildCBmap q dir
        string <- readFile $ dir ++ name ++ ".txt"
        let liste = parseSpec string
        return $ aux liste h
        where
                aux [] hm = hm
                aux ((n, stmt):tl) hm = HM.insert n stmt (aux tl hm)

-- refV, defV, mapping of a stim / variable to the set of influencing variables
parseCB :: Stmt -> (VarSet, VarSet, HM.Map Stim VarSet, HM.Map VarName VarSet, HM.Map VarName VarSet)
parseCB stmt = case stmt of
        Assign variable expression ->  do
                let referenced = lookForVinExpr expression
                (referenced, Set.insert variable Set.empty, HM.empty, HM.insert variable referenced HM.empty, Set.foldr (foldVinMap variable) HM.empty referenced)
        IfDo (G cond statement) -> do
                let (referenced, defined, stiMap, varMap, affVarMap) = parseCB statement
                let presentVariables = lookForVinCond cond
                let f v map = HM.map (Set.insert v) map -- insert v in all varset in a map
                let someFold someSet v hmap = case HM.lookup v hmap of
                        Nothing -> HM.insert v someSet hmap
                        Just set -> HM.insert v (Set.union set someSet) hmap
                (Set.union referenced presentVariables, defined, Set.foldr f stiMap presentVariables, Set.foldr f varMap presentVariables, Set.foldr (someFold defined) affVarMap presentVariables)
        IfDo (Alt (g:q)) -> do
                let (referenced, defined, stiMap, varMap, affVarMap) = parseCB $ IfDo $ Alt q
                let (refV, defV, stimap, varmap, affVar) = parseCB $ IfDo g
                (Set.union referenced refV, Set.union defined defV, HM.unionWith Set.union stiMap stimap, HM.unionWith Set.union varMap varmap, HM.unionWith Set.union affVarMap affVar)
        Seq (statement:q) -> do
                let (referenced, defined, stiMap, varMap, affVarMap) = parseCB $ Seq q
                let (refV, defV, stimap, varmap, affVar) = parseCB statement
                (Set.union referenced refV, Set.union defined defV, HM.unionWith Set.union stiMap stimap, HM.unionWith Set.union varMap varmap, HM.unionWith Set.union affVarMap affVar)
        Send expr -> do
                let stimuliSent = lookForVinExpr expr
                (Set.empty, Set.empty, Set.foldr createMapWithKeys HM.empty stimuliSent, HM.empty, HM.empty)
        _ -> (Set.empty, Set.empty, HM.empty, HM.empty, HM.empty)
        where
                lookForVinExpr expression = case expression of
                        LitteralExpr v -> Set.insert v Set.empty
                        OpExp e1 e2 -> Set.union (lookForVinExpr e1) (lookForVinExpr e2)
                lookForVinCond condition = case condition of
                        BoolConst _ -> Set.empty
                        BoolVar var -> Set.insert var Set.empty
                        Neg cond -> lookForVinCond cond
                        OpBool c1 c2 -> Set.union (lookForVinCond c1) (lookForVinCond c2)
                        OpComp e1 e2 -> Set.union (lookForVinExpr e1) (lookForVinExpr e2)
                createMapWithKeys key map = HM.insert key Set.empty map
                foldVinMap variable referenced hmap = case HM.lookup referenced hmap of
                        Nothing -> HM.insert referenced (Set.insert variable Set.empty) hmap
                        Just set -> HM.insert referenced (Set.insert variable set) hmap


buildInfluencingStimuli :: [Stim] -> CKASet -> (Stim -> CKA -> CKA) -> (CKA -> StimSet)
buildInfluencingStimuli stimlist behaviours nextBehamap = do
        let hm = Set.foldr (deriveInfluencingStimuli stimlist nextBehamap) HM.empty behaviours
        let inflS a = case (HM.lookup a hm) of
                Nothing -> Set.empty
                Just set -> set
        inflS
        where
                deriveInfluencingStimuli [] nextBehaMap a hashmap = hashmap
                deriveInfluencingStimuli (stim:q) nextBehaMap a hashmap = if nextBehaMap stim a == a
                        then deriveInfluencingStimuli q nextBehaMap a hashmap
                        else do
                                let h = deriveInfluencingStimuli q nextBehaMap a hashmap
                                let stimset = case (HM.lookup a h) of
                                        Nothing -> Set.empty
                                        Just set -> set
                                HM.insert a (Set.insert stim stimset) h

{-   FINAL FUNCTION  -}
buildRule :: FilePath -> IO(Rule)
buildRule dir = do
        fichier <- readFile $ dir ++ "AgentList.txt"
        let agents = lines fichier
        whichagent <- mapAgentName agents dir
        let agentList = map whichagent agents
        --available
        let availableBehaviours = buildAvailable agentList
        ckaMapping <- buildCBmap agents dir -- Map CKA Stmt
        let hm = HM.map parseCB ckaMapping -- Map CKA (refV, defV, Map Stim VarSet, Map VarName VarSet, Map VarName VarSet)
        --refV
        let referenced beha = case (HM.lookup beha hm) of
                Nothing -> Set.empty
                Just (refV, _, _, _, _) -> refV
        --defV
        let defined a = case (HM.lookup a hm) of
                Nothing -> Set.empty
                Just (_, defV, _, _, _) -> defV
        --stimList
        let stimlist = buildStimList agentList
        -- nextStim
        let nextStimulus = buildNextStimMap agentList
        -- nextBeha
        let nextBehavior = buildNextBehaMap agentList
        --infS
        let inflS = buildInfluencingStimuli stimlist (HM.keysSet hm) nextBehavior
        --probability distribution
        e <- tryJust (guard . isDoesNotExistError) (buildProbDistrib dir)
        let prob = either (const $ uniform availableBehaviours) id e
        -- affected variables
        let affV v cka = case HM.lookup cka hm of
                Nothing -> Set.empty
                Just (_, _, _, _, affVarMap) -> case HM.lookup v affVarMap of
                        Nothing -> Set.empty
                        Just set -> set
        let foldaux v cka hashmap = case HM.lookup cka hashmap of
                Nothing -> HM.insert cka (affV v cka) hashmap
                Just set -> HM.insert cka (Set.union set $ affV v cka) hashmap
        let foldManyBeh agent v hashmap = Set.foldr (foldaux v) hashmap $ availableBehaviours agent
        let proba agent f x set = (prob agent x) * (f set)
        let pAff agent f varset = HM.foldr (+) 0.0 $ HM.mapWithKey (proba agent f) $ Set.foldr (foldManyBeh agent) HM.empty varset
        return $ R whichagent availableBehaviours referenced defined inflS nextStimulus nextBehavior (final hm) stimlist prob pAff
        where
                uniform av agent behavior = do
                        let avBeha = av agent
                        if Set.member behavior avBeha
                                then 1.0 / (fromIntegral $ Set.size avBeha)
                                else 0.0
                final hm stim S a = case (HM.lookup a hm) of
                        Nothing -> Set.empty
                        Just (_, _, hs, _, _) -> case (HM.lookup stim hs) of
                                Nothing -> Set.empty
                                Just set -> set
                final hm var E a = case (HM.lookup a hm) of
                        Nothing -> Set.empty
                        Just (_, _, _, hv, _) -> case (HM.lookup var hv) of
                                Nothing -> Set.empty
                                Just set -> set