{- AUTHOR: Jason Jaskolka -}
-- Module containing the type definitions and parsers for the next-stimulus mapping of C2KA

module NextStimulus ( NSExpr (..) ,
                      NSMap       ,
					  exprParser  , 
                      parseNSMap   ) where

{- IMPORTS -}
import Behaviour ( CKA           ,
                   parseCKAExpr    )
import Stimulus  ( Stim          ,
                   parseStimExpr   )
				   
import Control.Applicative  ( (<*) )
import Text.Parsec          ( alphaNum ,
                              eof      ,
                              letter   ,
                              char     ,
                              parse    ,
                              oneOf    ,
                              (<|>)      )
import Text.Parsec.Language ( emptyDef )
import Text.Parsec.String   ( Parser )							  
import Text.Parsec.Token    ( GenLanguageDef (..) ,
                              GenTokenParser (..) ,
                              makeTokenParser       )				   

{- DATA TYPES -}
data NSExpr = NS Stim CKA Stim
	deriving (Eq)	

type NSMap = [NSExpr]

{- PARSER -}
-- NSExpr language grammar
grammar = emptyDef{ identStart      = letter                             ,
                    identLetter     = alphaNum <|> char ' ' <|> char '*' ,
                    opStart         = oneOf "(),="                       ,
                    opLetter        = oneOf "(),="                       ,
                    reservedOpNames = ["(",")",",","=",".", "*", "+"]    ,
                    reservedNames   = []                                   }					  

TokenParser{ parens     = m_parens     , 
             identifier = m_identifier , 
             reservedOp = m_reservedOp ,
             reserved   = m_reserved   ,
             semiSep1   = m_semiSep1   ,
             whiteSpace = m_whiteSpace   } = makeTokenParser grammar
	
exprParser :: Parser NSExpr
exprParser = do 
	m_reservedOp "("
	s <- m_identifier
	m_reservedOp ","
	a <- m_identifier
	m_reservedOp ")"
	m_reservedOp "="
	b <- m_identifier
	return $ NS s a b

mainParser :: Parser NSExpr
mainParser = m_whiteSpace >> exprParser <* eof

parseNSExpr :: String -> NSExpr
parseNSExpr input = case parse mainParser "" input of
	Left  err -> error $ show err
	Right ans -> ans   		  

parseNSMap :: [String] -> NSMap
parseNSMap = map parseNSExpr