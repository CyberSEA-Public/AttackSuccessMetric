{- AUTHOR: Jason Jaskolka -}
-- Module containing the type definitions and parsers for the next-behaviour mapping of C2KA

module NextBehaviour ( NBExpr (..) ,
                       NBMap       ,
					   exprParser  , 
                       parseNBMap   ) where
{- IMPORTS -}
import Behaviour ( CKA           ,
                   parseCKAExpr    )
import Stimulus  ( Stim          ,
                   parseStimExpr   )
				   
import Control.Applicative  ( (<*) )
import Text.Parsec          ( alphaNum ,
                              eof      ,
                              letter   ,
                              char     ,
                              parse    ,
                              oneOf    ,
                              (<|>)      )
import Text.Parsec.Language ( emptyDef )
import Text.Parsec.String   ( Parser )							  
import Text.Parsec.Token    ( GenLanguageDef (..) ,
                              GenTokenParser (..) ,
                              makeTokenParser       )				   

{- DATA TYPES -}
data NBExpr = NB Stim CKA CKA
	deriving (Eq)	

type NBMap = [NBExpr]

{- PARSER -}
-- NBExpr language grammar
grammar = emptyDef{ identStart      = alphaNum                ,
                    identLetter     = alphaNum              ,
                    opStart         = oneOf "(),="          ,
                    opLetter        = oneOf "(),="          ,
                    reservedOpNames = ["(",")",",","=","."] ,
                    reservedNames   = []                      }					  

TokenParser{ parens     = m_parens     , 
             identifier = m_identifier , 
             reservedOp = m_reservedOp ,
             reserved   = m_reserved   ,
             semiSep1   = m_semiSep1   ,
             whiteSpace = m_whiteSpace   } = makeTokenParser grammar

exprParser :: Parser NBExpr
exprParser = do 
	m_reservedOp "("
	s <- m_identifier
	m_reservedOp ","
	a <- m_identifier
	m_reservedOp ")"
	m_reservedOp "="
	b <- m_identifier
	return $ NB s a b

mainParser :: Parser NBExpr
mainParser = m_whiteSpace >> exprParser <* eof
	
parseNBExpr :: String -> NBExpr
parseNBExpr input = case parse mainParser "" input of
	Left  err -> error $ show err
	Right ans -> ans   		  

parseNBMap :: [String] -> NBMap
parseNBMap = map parseNBExpr