{- AUTHOR: Quentin Yang -}
-- Computes the Attack Success Metric for a set of implicit interactions

{- IMPORTS -}
import AttackSuccessReader
import Agent ( AgentName, Agent (name) )
import ConcreteSpec ( VarName )
import Stimulus ( Stim, StimSet )
import Behaviour( CKA, CKASet )

import qualified Data.Set as Set
import qualified Data.Map as HM
import Data.List
import Text.Printf
import System.Environment ( getArgs )

{- DATA TYPES -}
data Edge = Edge Agent CommType Agent

instance Show Edge where
    show (Edge a s b) = (name a) ++ " ->" ++ (show s) ++ " " ++ (name b)

-- Read a string and returns the corresponding Implicit interaction.
fromString :: Rule -> String -> [Edge]
fromString r s = aux r $ reverse $ words s
        where aux r split = case split of
                [] -> []
                [_] -> []
                (t:q) -> do
                        let comtype = if (head q) == "E" then E else S
                        (Edge ((lookupAgent r) t) comtype ((lookupAgent r) $ head $ tail q)) : (aux r $ tail q)

--- Print an implicit interaction
printInt :: [Edge] -> IO ()
printInt [] = return ()
printInt [Edge a s b] = putStr $ (name a) ++ "  ->" ++ (show s) ++ "  " ++ (name b)
printInt ((Edge a s b):q) = (putStr $ (name a) ++ "  ->" ++ (show s) ++ "  ") >> printInt q


data AttackSet = AS (StimSet) | AE (VarSet)
        deriving (Show)
isEmpty :: AttackSet -> Bool
isEmpty (AS set) = Set.null set
isEmpty (AE set) = Set.null set

{- ATTACK FUNCTION -}        
attack :: [Edge] -> Rule -> [AttackSet]
attack [] _ = []
attack [edge] r = case edge of
    Edge a S b -> [AS $ (getInfS r) b]
    Edge a E b -> [AE $ (getRefV r) b]
attack (edge:q) rule = case edge of
    Edge a S b -> do
        let queue = attack q rule
        if null queue
            then []
            else do
                case (head queue) of
                    AS stimset -> (AS $ Set.fromList [s | s <- stimList rule, Set.intersection (getNextStimSet s b rule) stimset /= Set.empty]):queue
                    AE varset -> (AS $ Set.fromList [s | s <- stimList rule, not $ Set.null $ Set.filter (filterVariable rule s varset) $ (available rule) b]):queue
    Edge a E b -> do
        let queue = attack q rule
        if null queue
            then []
            else do
                case (head queue) of
                    AS stimset -> (AE $ getInfV stimset S b rule):queue
                    AE varset -> (AE $ getInfV varset E b rule):queue
    where
                filterVariable rule stim varset x = do
                        let y = (nextBeha rule) stim x
                        (y /= x) && (not $ Set.null $ Set.intersection ((defV rule) y) varset)


{- ATTACK SUCCESS -}
attackSuccessDensity :: Edge -> AttackSet -> Rule -> Double
attackSuccessDensity edge (AS stimset) rule = case edge of
        Edge a E b -> 0.0
        Edge a S b -> do
                let avBeha = (available rule) a
                let n = Set.size avBeha
                if n == 0
                        then 0.0
                        else (fromIntegral $ Set.size $ Set.filter (\x-> not $ null $ filter (\s-> Set.member ((nextStim rule) s x) stimset) $ stimList rule) $ avBeha) / (fromIntegral n)
attackSuccessDensity edge (AE varset) rule = case edge of
    Edge a S b -> 0.0
    Edge a E b -> do
        let avBeha = (available rule) a
        let n = Set.size avBeha
        if n == 0
            then 0.0
            else (fromIntegral $ Set.size $ Set.filter (\x-> not $ Set.null $ Set.intersection ((defV rule) x) varset) avBeha) / (fromIntegral n)

attackSuccessDensityProduct :: [Edge] -> Rule -> Double
attackSuccessDensityProduct liste rule = do
        let attacklist = attack liste rule
        if (null attacklist) || (isEmpty $ head attacklist)
                then 0.0
                else aux (tail attacklist) (tail liste) rule
        where
                aux [] [] _ = 1.0
                aux [] _ _ = 0.0
                aux _ [] _ = 0.0
                aux [atk] [edge] rule = attackSuccessDensity edge atk rule
                aux (atk:qa) (e:qe) rule = (attackSuccessDensity e atk rule) * (aux qa qe rule)

--Probability that the an agent (the sink agent) is affected by a stim while following the specified rule.
pAffect :: Rule -> Agent -> Stim -> Double --used, only once
pAffect r a s = foldr (+) 0.0 $ map (\x-> if (nextBeha r) s x == x then 0.0 else (probDistrib r) a x) $ Set.toList $ (available r) a

-- just some lazy alias
lookupValue hmap key = case HM.lookup key hmap of --used
        Nothing -> 0.0
        Just value -> value
lookupMap hmap v = case HM.lookup v hmap of --used
        Nothing -> HM.empty
        Just h -> h
lookupSet hmap k = case HM.lookup k hmap of
        Nothing -> Set.empty
        Just set -> set

--map each key with a corresponding value
foldinMap f s map = HM.insert s (f s) map --used

-- probability that an agent affects the sink through the interaction while stimulated by a given stimulus.
pPropS :: Rule -> Agent -> HM.Map Stim Double -> Stim -> Double --used
pPropS r agent hmap s = foldr (+) 0.0 $ map (\x-> (*) (lookupValue hmap $ (nextStim r) s x) $ (probDistrib r) agent x) $ Set.toList $ (available r) agent where

-- map each variable to the set of CKA it can affect
buildAffMap :: Rule -> Agent -> HM.Map VarName CKASet --used, only once
buildAffMap rule agent = Set.foldr (foldManyBeh rule) HM.empty $ (available rule) agent where
        foldManyBeh rule cka hmap = Set.foldr (foldManyVar cka) hmap $ (refV rule) cka
        foldManyVar cka v hmap = case HM.lookup v hmap of
                Nothing -> HM.insert v (Set.insert cka Set.empty) hmap
                Just set -> HM.insert v (Set.insert cka set) hmap

-- given a mapping VarSet->Double, mapp each stimulus to its corresponding probability.
foldmanyS :: Rule -> (VarSet -> Double) -> Agent -> Stim -> Double --used
foldmanyS r probMap agent s = foldr (+) 0.0 $ map (pPropV r probMap agent s) $ Set.toList $ (available r) agent where
        pPropV r probMap agent s x = do
                let next = (nextBeha r) s x
                if next == x then 0.0 else (probMap $ (defV r) next) * ((probDistrib r) agent x)

data InteractionType =
        Sti (HM.Map Stim Double)
        |Var (VarSet -> Double)

instance Show InteractionType where
        show (Var _) = "Var f"
        show (Sti hm) = show hm

-- superMap :: HM.Map Varname $ HM.Map CKA $ HM.map Stim Double
deriveProbS agent rule superMap varset = HM.foldrWithKey (plusAgent rule agent) 0.0 $ HM.map (HM.foldr (+) 0.0) $ foldr (HM.unionWith $ HM.unionWith max) HM.empty $ map (lookupMap superMap) $ Set.toList varset where
        plusAgent :: Rule -> Agent -> CKA -> Double -> Double -> Double
        plusAgent rule agent cka p q = q + p  * ( (probDistrib rule) agent cka )


attackSuccess :: Rule -> [Edge] -> Double
attackSuccess _ [] = 0.0
attackSuccess rule liste = case aux liste rule of
        Sti attackMap -> HM.foldr max 0.0 attackMap
        Var probMap -> probMap $ Set.foldr Set.union Set.empty $ Set.map (refV rule) $ (available rule) $ cible liste
        where
                cible liste = case (head liste) of
                        Edge a t b -> b
                aux [Edge a S b] rule = Sti $ foldr (foldinMap $ pAffect rule b) HM.empty $ stimList rule -- ok
                aux [Edge a E b] rule = do
                        let affectMap = buildAffMap rule b
                        let pAff varset = foldr (+) 0.0 $ map ((probDistrib rule) b) $ Set.toList $ Set.foldr Set.union Set.empty $ Set.map (lookupSet affectMap) varset
                        Var pAff --ok
                aux ((Edge a S b):q) rule = case aux q rule of
                        Sti attackMap -> Sti $ foldr (foldinMap $ pPropS rule b attackMap) HM.empty $ stimList rule --ok
                        Var probMap -> Sti $ foldr (foldinMap $ foldmanyS rule probMap b) HM.empty $ stimList rule --ok
                aux ((Edge a E b):q) rule = case aux q rule of
                        Sti attackMap -> do
                                let superMap = HM.foldrWithKey (foldAuxStim rule b) HM.empty attackMap
                                Var $ deriveProbS b rule superMap --ok
                        Var probMap -> Var $ (probAffect rule) b probMap --ok
                foldAuxStim r agent s p hmap = Set.foldr (foldAuxBeha r s p) hmap $ (available r) agent
                foldAuxBeha r s p a hmap = Set.foldr (foldAuxVar s p a) hmap $ ((inflV r) s S a)
                foldAuxVar s p a v hmap = case HM.lookup v hmap of
                        Nothing -> HM.insert v (HM.insert a (HM.insert s p HM.empty) HM.empty) hmap
                        Just h -> case HM.lookup a h of
                                Nothing -> HM.insert v (HM.insert a (HM.insert s p HM.empty) h) hmap
                                Just hs -> do
                                        let ps = case HM.lookup s hs of
                                                Nothing -> p
                                                Just prob -> max p prob
                                        HM.insert v (HM.insert a (HM.insert s ps hs) h) hmap

{- MAIN FUNCTION -}
-- Computes the attack success metric for the systems specified in the command line parameter
main :: IO()
main = do
        args <- getArgs
        let epsilon = if length args > 1 then read (args!!1) else 0.0
        let dir = "Examples/" ++ args!!0 ++ "/"
        rule <- buildRule dir
        args <- readFile $ dir ++ "Implicits.txt"
        let implicit = map (fromString rule) $ lines args
        aux epsilon implicit rule
        where
                aux _ [] rule = return ()
                aux epsilon (edges:q) rule = do
                        let p = attackSuccess rule edges
                        if p > epsilon then do
                                putStr "Implicit Interaction:  "								
                                printInt edges
                                putStr "\nProbability of Attack Success <= "
                                putStrLn $ printf "%.4f" p
                                else putStr ""
                        aux epsilon q rule
