{- AUTHOR: Jason Jaskolka -}
-- Module containing the type definitions and parsers for agents and agent specifications

module Agent ( AgentName      ,
               AgentSpec (..) ,
               Agent (..)     ,
               Eq (..)        ,
               Ord (..)       ,
			   parseAgent     ,
			   load             ) where

{- IMPORTS -}
import Behaviour     ( CKAExpr, exprParser )
import NextStimulus  ( NSMap, exprParser )
import NextBehaviour ( NBMap, exprParser )
import ConcreteSpec  ( CBSpec, specParser )

import Control.Applicative  ( (<*) )
import Text.Parsec          ( alphaNum ,
                              eof      ,
                              letter   ,
                              parse    ,
                              oneOf    ,
                              many1      )
import Text.Parsec.Language ( emptyDef )
import Text.Parsec.String   ( Parser )							  
import Text.Parsec.Token    ( GenLanguageDef (..) ,
                              GenTokenParser (..) ,
                              makeTokenParser       )

import System.IO.Error ( catchIOError )

{- DATA TYPES -}
type AgentName = String

data AgentSpec = AgentSpec { expr   :: CKAExpr ,
                             nbmap  :: NBMap   ,
                             nsmap  :: NSMap   ,
                             cbspec :: CBSpec    }

data Agent = Agent { name :: AgentName ,
                     spec :: AgentSpec   }

instance Eq (Agent) where
	(Agent a _) == (Agent b _) = a == b

instance Ord (Agent) where
	(Agent a _) `compare` (Agent b _) = a `compare` b

{- PARSER -}
{-A parser for agent specification files of the following form:
begin AGENT where
	AgentName := CKAExpr
end

begin NEXT_BEHAVIOUR where
    (Stim,CKA) = CKA
end

begin NEXT_STIMULUS where
    (Stim,CKA) = Stim
end

begin CONCRETE_BEHAVIOUR where
    CKA => [ Stmt ]
end
-}
-- Agent specification language grammar
grammar = emptyDef{ identStart      = letter                            ,
                    identLetter     = alphaNum                          ,
                    opStart         = oneOf ":"                         ,
                    opLetter        = oneOf "="                         ,
                    reservedOpNames = [":="]                            ,
                    reservedNames   = ["begin", "end", "where", "AGENT" , 
                                       "NEXT_BEHAVIOUR", "NEXT_STIMULUS", 
                                       "CONCRETE_BEHAVIOUR"              ] }		

TokenParser{ parens     = m_parens     , 
             identifier = m_identifier , 
             reservedOp = m_reservedOp ,
             reserved   = m_reserved   ,
             semiSep1   = m_semiSep1   ,
             whiteSpace = m_whiteSpace   } = makeTokenParser grammar

agentParser :: Parser Agent
agentParser = do 
	m_reserved "begin"
	m_reserved "AGENT"
	m_reserved "where"
	name  <- m_identifier
	m_reservedOp ":="
	term  <- Behaviour.exprParser
	m_reserved "end"
	m_reserved "begin"
	m_reserved "NEXT_BEHAVIOUR"
	m_reserved "where"
	nbmap <- many1 NextBehaviour.exprParser
	m_reserved "end"
	m_reserved "begin"
	m_reserved "NEXT_STIMULUS"
	m_reserved "where"
	nsmap <- many1 NextStimulus.exprParser
	m_reserved "end"
	m_reserved "begin"
	m_reserved "CONCRETE_BEHAVIOUR"
	m_reserved "where"
	specs <- many1 specParser
	m_reserved "end"
	return $ Agent name (AgentSpec term nbmap nsmap specs)

mainParser :: Parser Agent
mainParser = m_whiteSpace >> agentParser <* eof

parseAgent :: String -> Agent
parseAgent input = case parse mainParser "" input of
	Left  err -> error $ show err
	Right ans -> ans   		  

-- Read the agent specification file at the given FilePath. If the read occurs without error, load returns the parsed Agent, otherwise it returns an error message.
load :: FilePath -> IO Agent
load agent = catchIOError (readFile agent) (errorMsg agent) >>= \a -> return $ parseAgent a
	where
		errorMsg :: FilePath -> IOError -> IO String
		errorMsg fp = error $ "File " ++ fp ++ " does not exist!" 