{- AUTHOR: Jason Jaskolka -}
-- Module containing the type definitions and parsers for cocnrete behaviour specifications using Dijkstra's guaraded command language.

module ConcreteSpec ( VarName    ,
                      Constants  ,
                      Expr (..)  ,
                      Cond (..)  ,
                      Guard (..) ,
                      Stmt (..)  ,                             
                      Spec (..)  ,
                      CBSpec     ,
					  specParser ,
                      parseSpec       ) where

{- IMPORTS -}
import Data.Set (Set)	
import Control.Applicative  ( (<*) )
import Text.Parsec          ( alphaNum ,
                              eof      ,
                              letter   ,
                              parse    ,
                              oneOf    ,
                              sepBy1   ,							  
                              (<?>)    , 
                              (<|>)      )
import Text.Parsec.Expr     ( Assoc (AssocLeft)     ,
                              Operator (Infix)      , 
                              Operator (Prefix)     ,
                              buildExpressionParser   )
import Text.Parsec.Language ( emptyDef )
import Text.Parsec.String   ( Parser )							  
import Text.Parsec.Token    ( GenLanguageDef (..) ,
                              GenTokenParser (..) ,
                              makeTokenParser       )

{- DATA TYPES -}
type BehaviourName = String

type VarName = String

type Constant  = String

type Constants = Set Constant

data Expr = Val Constant   | 
            Add Expr Expr  |
            Sub Expr Expr  |
            Mul Expr Expr  |
            Div Expr Expr  

data Cond = T              |
            F              |  
            Con Constant   |
            Neg Cond       |
            And Cond Cond  |
            Or  Cond Cond  |
            Eq  Cond Cond  |
            Gt Cond Cond   |
            Lt Cond Cond   |
            Geq Cond Cond  |
            Leq Cond Cond  

data Guard = G Cond Stmt  |
             Alt [Guard]

data Stmt = Abort           |
            Skip            |
            VarName := Expr | 
            Seq [Stmt]      |
            If Guard        |
            Do Guard        |
            Send Expr       |
            Receive Expr  			

data Spec = BehaviourName ::= Stmt

type CBSpec = [Spec]

{- PARSER -}
-- Spec language grammar
grammar = emptyDef{ identStart      = alphaNum               ,
                    identLetter     = alphaNum               ,
                    opStart         = oneOf "=:+-/*[]|~&"    ,
                    opLetter        = oneOf "=>&|"           ,
                    reservedOpNames = ["+", "-", "*", "/"    , 
                                       ":=", "=>", "->", "|" , 
                                       "~", "&&", "||"       , 
                                       "[", "]", "()"]       ,
                    reservedNames   = ["abort", "skip"       , 
                                       "if", "fi"            , 
                                       "do", "od"            , 
                                       "send", "receive"     ,
                                       "true", "false"       , 
                                       "end"]                  }			 

TokenParser{ parens     = m_parens     ,
             identifier = m_identifier , 
             reservedOp = m_reservedOp ,
             reserved   = m_reserved   ,
             semiSep1   = m_semiSep1   ,
             whiteSpace = m_whiteSpace   } = makeTokenParser grammar

condParser :: Parser Cond
condParser = buildExpressionParser table term <?> "Cond"
	where
		table = [ [Prefix (m_reservedOp "~"   >> return (Neg))  ]          ,
		          [Infix  (m_reservedOp "&&"  >> return (And))  AssocLeft] ,
		          [Infix  (m_reservedOp "||"  >> return (Or))   AssocLeft] ,
		          [Infix  (m_reservedOp "="   >> return (Eq))   AssocLeft] ,
		          [Infix  (m_reservedOp ">"   >> return (Gt))   AssocLeft] ,
		          [Infix  (m_reservedOp "<"   >> return (Lt))   AssocLeft] ,
		          [Infix  (m_reservedOp ">="  >> return (Geq))  AssocLeft] ,
		          [Infix  (m_reservedOp "<="  >> return (Leq))  AssocLeft]   ]
		term  = m_parens condParser
		        <|> fmap Con m_identifier 
		        <|> (m_reserved "true"  >> return (T))
		        <|> (m_reserved "false" >> return (F))

exprParser :: Parser Expr
exprParser = buildExpressionParser table term <?> "Expr"
	where
		table = [ [Infix (m_reservedOp "+"  >> return (Add)) AssocLeft] ,
		          [Infix (m_reservedOp "-"  >> return (Sub)) AssocLeft] ,
		          [Infix (m_reservedOp "*"  >> return (Mul)) AssocLeft] ,
		          [Infix (m_reservedOp "/"  >> return (Div)) AssocLeft]   ]
		term  = m_parens exprParser
		        <|> fmap Val m_identifier

guardParser :: Parser Guard
guardParser = fmap Alt (sepBy1 guard (m_reservedOp "|"))
	where
		guard = do
			c <- condParser
			m_reservedOp "->"
			s <- stmtParser
			return (G c s)	
	
stmtParser :: Parser Stmt
stmtParser = fmap Seq (m_semiSep1 stmt)
	where
		stmt = (m_reserved "abort" >> return (Abort))
			<|>  (m_reserved "skip"  >> return (Skip))
			<|> do 
				v <- m_identifier
				m_reservedOp ":="
				e <- exprParser
				return (v := e)
			<|> do 
				m_reserved "if"
				g <- guardParser
				m_reserved "fi"
				return (If g)
			<|> do
				m_reserved "do"
				g <- guardParser
				m_reserved "od"
				return (Do g)
			<|> do
				m_reserved "send"
				e <- exprParser
				return (Send e)
			<|> do
				m_reserved "receive"
				e <- exprParser
				return (Receive e)				

specParser :: Parser Spec
specParser = do 
	n <- m_identifier
	m_reservedOp "=>"
	m_reservedOp "["
	s <- stmtParser
	m_reservedOp "]"
	return (n ::= s)

mainParser :: Parser Spec
mainParser = m_whiteSpace >> specParser <* eof	

parseSpec :: String -> Spec
parseSpec input = case parse mainParser "" input of
	Left  err -> error $ show err
	Right ans -> ans   			  