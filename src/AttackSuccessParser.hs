{- AUTHOR: Quentin Yang -}
-- Parser for Diskstra's guarded command laguage for use in computing the Attack Success Metric

module AttackSuccessParser ( parseSpec, Expr ( .. ), Cond ( .. ), Stmt ( .. ), Guard ( .. ) ) where

{- IMPORTS -}
import Control.Applicative  ( (<*) )
import Text.Parsec ( alphaNum ,
                     eof      ,
                     letter   ,
                     parse    ,
                     oneOf    ,
                     sepBy1   ,
                     many1    ,
                     (<?>)    , 
                     (<|>)      )
import Text.Parsec.Expr     ( Assoc (AssocLeft)     ,
                              Operator (Infix)      , 
                              Operator (Prefix)     ,
                              buildExpressionParser   )
import Text.Parsec.Language ( emptyDef )
import Text.Parsec.String   ( Parser )
import Text.Parsec.Token    ( GenLanguageDef (..) ,
                              GenTokenParser (..) ,
                              makeTokenParser       )
import Behaviour     ( exprParser )
import NextStimulus  ( exprParser )
import NextBehaviour (exprParser )
import Data.HashMap

{- DATA TYPES -}
data Expr =
        LitteralExpr String
        |OpExp Expr Expr
        deriving (Show)

data Cond =
        BoolConst Bool
        |BoolVar String
        |Neg Cond
        |OpBool Cond Cond
        |OpComp Expr Expr
        deriving (Show)

data Stmt =
        Assign String Expr -- v := expr || v1, ..., vn = e1, ... en
        |IfDo Guard
        |Seq [Stmt]
        |Send Expr
        |Special --skip, abort and receive
        deriving (Show)

data Guard = G Cond Stmt
        |Alt [Guard]
        deriving (Show)

{- PARSING FUNCTIONS -}
agentGrammar = emptyDef { identStart = letter,
                     identLetter = alphaNum,
                     opStart = oneOf ":",
                     opLetter = oneOf "=",
                     reservedOpNames = [":="],
                     reservedNames = ["begin", "end", "where", "AGENT" , 
                                       "NEXT_BEHAVIOUR", "NEXT_STIMULUS", 
                                       "CONCRETE_BEHAVIOUR"              ]
                    }
agentLexer = makeTokenParser agentGrammar

agentParser :: Parser [(String, Stmt)]
agentParser = do
        (reserved agentLexer) "begin"
        (reserved agentLexer) "AGENT"
        (reserved agentLexer) "where"
        nom <- identifier agentLexer
        (reservedOp agentLexer) ":="
        _ <- Behaviour.exprParser
        (reserved agentLexer) "end"
        (reserved agentLexer) "begin"
        (reserved agentLexer) "NEXT_BEHAVIOUR"
        (reserved agentLexer) "where"
        _ <- many1 NextBehaviour.exprParser
        (reserved agentLexer) "end"
        (reserved agentLexer) "begin"
        (reserved agentLexer) "NEXT_STIMULUS"
        (reserved agentLexer) "where"
        _ <- many1 NextStimulus.exprParser
        (reserved agentLexer) "end"
        (reserved agentLexer) "begin"
        (reserved agentLexer) "CONCRETE_BEHAVIOUR"
        (reserved agentLexer) "where"
        s <- many1 specParser
        (reserved agentLexer) "end"
        return s
            
languageDef = emptyDef { identStart      = alphaNum               ,
                    identLetter     = alphaNum               ,
                    opStart         = oneOf "=:+-/*[]|~&"    ,
                    opLetter        = oneOf "=>&|"           ,
                    reservedOpNames = ["+", "-", "*", "/"    , 
                                       ":=", "=>", "->", "|" , 
                                       "~", "&&", "||"       , 
                                       "[", "]", "()", "(", ")"]       ,
                    reservedNames   = ["abort", "skip"       , 
                                       "if", "fi"            , 
                                       "do", "od"            , 
                                       "send", "receive"     ,
                                       "true", "false",
                                       "NEXT_BEHAVIOUR", "NEXT_STIMULUS", "CONCRETE_BEHAVIOUR", "end", "where"      ]                  }

TokenParser{ parens     = m_parens     ,
             identifier = m_identifier , 
             reservedOp = m_reservedOp ,
             reserved   = m_reserved   ,
             semiSep1   = m_semiSep1   ,
             whiteSpace = m_whiteSpace   } = makeTokenParser languageDef

stmtParser :: Parser Stmt
stmtParser = fmap Seq (m_semiSep1 stmt)
        where
                stmt = (m_reserved "abort" >> return Special)
                        <|>  (m_reserved "skip"  >> return Special)
                        <|> do 
                                v <- m_identifier
                                m_reservedOp ":="
                                e <- exParser
                                return (Assign v e)
                        <|> do 
                                m_reserved "if"
                                g <- guardParser
                                m_reserved "fi"
                                return (IfDo g)
                        <|> do
                                m_reserved "do"
                                g <- guardParser
                                m_reserved "od"
                                return (IfDo g)
                        <|> do
                                m_reserved "send"
                                e <- exParser
                                return (Send e)
                        <|> do
                                m_reserved "receive"
                                e <- exParser
                                return (Special)

exParser :: Parser Expr
exParser = buildExpressionParser table term <?> "Expr"
        where
                table = [ [Infix (m_reservedOp "+"  >> return (OpExp)) AssocLeft] ,
                        [Infix (m_reservedOp "-"  >> return (OpExp)) AssocLeft] ,
                        [Infix (m_reservedOp "*"  >> return (OpExp)) AssocLeft] ,
                        [Infix (m_reservedOp "/"  >> return (OpExp)) AssocLeft]   ]
                term  = m_parens exParser
                        <|> fmap LitteralExpr m_identifier

guardParser :: Parser Guard
guardParser = fmap Alt $ sepBy1 guard $ m_reservedOp "|"
        where
                guard = do
                        c <- condParser
                        m_reservedOp "->"
                        s <- stmtParser
                        return $ G c s


condParser :: Parser Cond
condParser = buildExpressionParser table term <?> "Cond"
        where
                table = [ [Prefix (m_reservedOp "~"   >> return (Neg))  ]          ,
                          [Infix  (m_reservedOp "&&"  >> return (OpBool))  AssocLeft] ,
                          [Infix  (m_reservedOp "||"  >> return (OpBool))   AssocLeft]
                        ]
                term  = m_parens condParser
                        <|> (m_reserved "true"  >> return (BoolConst True))
                        <|> (m_reserved "false" >> return (BoolConst False))
                        <|> readExpression
                        <|> fmap BoolVar m_identifier
                readExpression = do
                        a1 <- exParser
                        relation
                        a2 <- exParser
                        return $ OpComp a1 a2
                relation = m_reservedOp "="
                        <|> m_reservedOp ">"
                        <|> m_reservedOp "<"
                        <|> m_reservedOp ">="
                        <|> m_reservedOp "<="

specParser :: Parser (String, Stmt)
specParser = do 
        n <- m_identifier       
        m_reservedOp "=>"
        m_reservedOp "["
        s <- stmtParser
        m_reservedOp "]"
        return (n, s)

mainParser :: Parser [(String, Stmt)]
mainParser = m_whiteSpace >> agentParser <* eof

parseSpec :: String -> [(String, Stmt)]
parseSpec input = case parse mainParser "" input of
        Left  err -> error $ show err
        Right ans -> ans  