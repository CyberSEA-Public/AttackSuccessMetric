{- AUTHOR: Jason Jaskolka -}
-- Module containing the type definitions and parsers for CKA behaviours

module Behaviour ( CKA          ,
                   CKASet       ,
                   CKAExpr (..) ,
				   parseCKAExpr , 
                   exprParser   ,
				   extractCKA    ) where

{- IMPORTS -}
import Data.Set as Set   ( Set,
						   empty      , 
                           singleton  , 
                           union      )

import Control.Applicative  ( (<*) )
import Text.Parsec          ( alphaNum ,
                              eof      ,
                              letter   ,
                              parse    ,
                              oneOf    , 
                              (<?>)    , 
                              (<|>)      )
import Text.Parsec.Expr     ( Assoc (AssocLeft)         , 
                              Operator (Infix, Postfix) ,
                              buildExpressionParser       )
import Text.Parsec.Language ( emptyDef )
import Text.Parsec.String   ( Parser )							  
import Text.Parsec.Token    ( GenLanguageDef (..) ,
                              GenTokenParser (..) ,
                              makeTokenParser       )

{- DATA TYPES -}
type CKA = String	

type CKASet = Set CKA	

data CKAExpr = 
	Z                        |
	O                        |
	Literal CKA              |
	Choice  CKAExpr CKAExpr  | 
	Seq     CKAExpr CKAExpr  | 
	Par     CKAExpr CKAExpr  | 
	IterSeq CKAExpr          |
	IterPar CKAExpr				
		deriving (Eq)	

{- PARSER -}
-- CKAExpr language grammar
grammar = emptyDef{ identStart      = letter                      ,
                    identLetter     = alphaNum                    ,
                    opStart         = oneOf "+*;^"                ,
                    opLetter        = oneOf "+*;^"                ,
                    reservedOpNames = ["+", ";", "*", "^;", "^*"] ,
                    reservedNames   = ["1", "0"]                    }	

TokenParser{ parens     = m_parens     , 
             identifier = m_identifier , 
             reservedOp = m_reservedOp ,
             reserved   = m_reserved   ,
             semiSep1   = m_semiSep1   ,
             whiteSpace = m_whiteSpace   } = makeTokenParser grammar

exprParser :: Parser CKAExpr
exprParser = buildExpressionParser table term <?> "CKAExpr"
	where
		table = [ [Infix   (m_reservedOp "+"  >> return (Choice))  AssocLeft] ,
		          [Infix   (m_reservedOp ";"  >> return (Seq))     AssocLeft] ,
		          [Infix   (m_reservedOp "*"  >> return (Par))     AssocLeft] ,
		          [Postfix (m_reservedOp "^;" >> return (IterSeq))          ] ,
		          [Postfix (m_reservedOp "^*" >> return (IterPar))          ]   ]
		term  = m_parens exprParser
		        <|> fmap Literal m_identifier
		        <|> (m_reserved "1" >> return (O))
		        <|> (m_reserved "0" >> return (Z))

mainParser :: Parser CKAExpr
mainParser = m_whiteSpace >> exprParser <* eof	

parseCKAExpr :: String -> CKAExpr
parseCKAExpr input = case parse mainParser "" input of
	Left  err -> error $ show err
	Right ans -> ans  
	
	
{- OTHER FUNCTIONS -}
-- Extracts the set of all of the atomic CKA behaviours in a given CKAExpr
extractCKA :: CKAExpr -> CKASet
extractCKA Z            = Set.empty
extractCKA O            = Set.empty
extractCKA (Literal a)  = Set.singleton a
extractCKA (Choice a b) = extractCKA a `union` extractCKA b
extractCKA (Seq a b)    = extractCKA a `union` extractCKA b
extractCKA (Par a b)    = extractCKA a `union` extractCKA b
extractCKA (IterSeq a)  = extractCKA a
extractCKA (IterPar a)  = extractCKA a